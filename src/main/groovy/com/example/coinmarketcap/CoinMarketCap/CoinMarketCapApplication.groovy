package com.example.coinmarketcap.CoinMarketCap

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
@ComponentScan("coinmarketcapbot")
class CoinMarketCapApplication {

	static void main(String[] args) {
		SpringApplication.run(CoinMarketCapApplication, args)
	}

}
