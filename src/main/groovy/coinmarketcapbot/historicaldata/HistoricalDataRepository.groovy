package coinmarketcapbot.historicaldata

import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository

@Repository
@Slf4j
class HistoricalDataRepository {

  private final JdbcTemplate jdbcTemplate

  private final RowMapper<HistoricalData> rowMapper = new HistoricalDataEntityRowMapper()

  @Autowired
  HistoricalDataRepository(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate
  }

  void createHistoricalDataForADay(HistoricalData historicalData) {
    def sql = '''
     EXECUTE createHistoricalData
        @refID_Cryptocurrencies=?,
        @datetime=?,
        @opened=?,
        @closed=?,
        @high=?,
        @low=?,
        @volume24h=?,
        @marketcap=?'''


    jdbcTemplate.update(
      sql,
      historicalData.refID_Cryptocurrencies,
      historicalData.localDate,
      historicalData.opened,
      historicalData.closed,
      historicalData.high,
      historicalData.low,
      historicalData.volume24h,
      historicalData.marketCap)
  }

  HistoricalData checkIfDataAlreadyExists(HistoricalData historicalData) {
    def sql = '''
    EXECUTE checkForExistingHistData
     @datetime=?,
     @refID_Cryptocurrencies=?'''

    try {
      jdbcTemplate.queryForObject(
        sql,
        rowMapper,
        historicalData.localDate,
        historicalData.refID_Cryptocurrencies)

    } catch (EmptyResultDataAccessException e) {
      log.info("No such information in DB for date ${historicalData.localDate}", e.message)
      return null
    }
  }

  List<HistoricalData> getFullHistoricalDataList() {
    def sql =
      'EXECUTE getFullListHistoricalData '

    jdbcTemplate.query(sql, rowMapper)
  }

  List<HistoricalData> getHistoricalDataByDate(Long currencyId, String start, String end) {
    def sql = '''
      EXECUTE getHistoricalDataByDate
      @refID_Cryptocurrencies =?,
      @startDate=?,
      @endDate= ? '''

    jdbcTemplate.query(
      sql,
      rowMapper,
      currencyId,
      start,
      end)
  }

}
