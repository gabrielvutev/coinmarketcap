package coinmarketcapbot.historicaldata

import org.springframework.jdbc.core.RowMapper

import java.sql.ResultSet
import java.sql.SQLException

class HistoricalDataEntityRowMapper implements RowMapper<HistoricalData> {


  @Override
  HistoricalData mapRow(ResultSet resultSet, int i) throws SQLException {
    HistoricalData entity = new HistoricalData(
      id: resultSet.getLong('id'),
      refID_Cryptocurrencies: resultSet.getLong('refID_Cryptocurrencies'),
      localDate: resultSet.getDate('datetime').toLocalDate(),
      opened: resultSet.getFloat('opened'),
      high: resultSet.getFloat('high'),
      low: resultSet.getFloat('low'),
      closed: resultSet.getFloat('closed'),
      volume24h: resultSet.getFloat('volume24h'),
      marketCap: resultSet.getFloat('marketCap')
    )
     entity
  }
}
