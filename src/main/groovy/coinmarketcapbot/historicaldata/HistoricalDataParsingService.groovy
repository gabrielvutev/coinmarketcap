package coinmarketcapbot.historicaldata

import coinmarketcapbot.cryptocurrencies.CryptocurrenciesEntity
import coinmarketcapbot.cryptocurrencies.CryptocurrenciesService
import groovy.util.logging.Slf4j
import org.apache.http.HttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.util.EntityUtils
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.apache.http.client.HttpClient
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@Service
@Slf4j
class HistoricalDataParsingService {

  private final String indexPageUrl = 'https://coinmarketcap.com/'

  private final String row = 'cmc-table-row'

  private final int cellsSize = 7

  private final CryptocurrenciesService cryptocurrenciesService

  private final HistoricalDataRepository historicalDataRepository

  private final HttpClient httpClient = HttpClientBuilder.create().build()

  @Autowired
  HistoricalDataParsingService(
    CryptocurrenciesService cryptocurrenciesService,
    HistoricalDataRepository historicalDataRepository
  ) {
    this.cryptocurrenciesService = cryptocurrenciesService
    this.historicalDataRepository = historicalDataRepository
  }

  void parseHistoricalDataByName(String name, String startDate, String endDate) {
    log.info("Starting process for parsing historical data for cryptocurrency ${name}")
    Elements rows = getAllHistoricalDataPageRows(name, startDate, endDate)
    if (!rows) return

    rows.each {
      row ->
        HistoricalData dataForADay = extractSingleDataForADay(row, name)
        HistoricalData data = checkIfHistoricalDataForSpecificDayExists(dataForADay)
        if (!data) {
          this.historicalDataRepository.createHistoricalDataForADay(dataForADay)
          log.info("Persisted historical info in DB for the date ${dataForADay.localDate}")
        }
    }
  }

  List<HistoricalData> getHistoricalDataByCurrencyForAllTime(String currency, String endDate) {
    CryptocurrenciesEntity entity = cryptocurrenciesService.getEntityByName(currency)
    historicalDataRepository.getHistoricalDataByCurrencyForAllTime(currency, endDate)
  }

  void parseHistoricalDataById(Long currencyId) {
    CryptocurrenciesEntity cryptocurrency = cryptocurrenciesService.getEntityById(currencyId)
    if (cryptocurrency) {
      log.info("Cryptocurrency with id ${currencyId} has been FOUND ")
      parseHistoricalDataByName(cryptocurrency.name)
    }
  }

  List<HistoricalData> getHistoricalDataByStartEndDate(String currency, String startDate, String endDate) {
    CryptocurrenciesEntity cryptocurrenciesEntity = checkIfCryptocurrencyExists(currency)
    if (!cryptocurrenciesEntity) {
      return null
    }

    List<HistoricalData> returnedFromQueryHistoricalData =
      historicalDataRepository.getHistoricalDataByDate(cryptocurrenciesEntity.id, startDate, endDate)

    if (!returnedFromQueryHistoricalData) {
      log.info("Invalid date provided for cryptocurrency ${cryptocurrenciesEntity.name}")
      return null
    }
    return returnedFromQueryHistoricalData
  }

  List<HistoricalData> getAllHistoricalData() {
    historicalDataRepository.getFullHistoricalDataList()
  }


  List<HistoricalData> getHistoricalDataById(Long id, String startDate, String endDate) {
    List<HistoricalData> dataFromDb = historicalDataRepository.getHistoricalDataByDate(id, startDate, endDate)
    if (!dataFromDb) {
      log.info("The provided id ${id} still does not exists in DB")
      return null
    }
    return dataFromDb
  }

  private CryptocurrenciesEntity checkIfCryptocurrencyExists(String name) {
    CryptocurrenciesEntity cryptocurrency = cryptocurrenciesService.getEntityByName(name)
    if (!cryptocurrency) {
      log.info("Cryptocurrency with name ${name} does not exist")
      return null
    }
    return cryptocurrency
  }

  private HistoricalData checkIfHistoricalDataForSpecificDayExists(HistoricalData historicalData) {
    HistoricalData retrievedFromDBHistorData = this.historicalDataRepository.checkIfDataAlreadyExists(historicalData)
    if (retrievedFromDBHistorData) {
      log.info(
        "There is already information in DB for Cryptocurrency with id: " +
          " ${retrievedFromDBHistorData.refID_Cryptocurrencies} for" +
          " the day ${retrievedFromDBHistorData.localDate}")
      return retrievedFromDBHistorData
    }
     retrievedFromDBHistorData
  }

  private Elements getAllHistoricalDataPageRows(String currency, String start, String end) {
    Document document = getHistoricalDataPageDocument(currency,start,end)
    Elements rows = document.getElementsByClass(row)

    if (!rows) {
      log.error("Failed to locate  page rows for cryptocurrency  ${currency}")
      return null
    }
     rows
  }

  private Document getHistoricalDataPageDocument(String currency, String start, String end) {
    String htmlString = getHistoricalDataPageHtml(currency,start, end)
     Jsoup.parse(htmlString)
  }

  private String getHistoricalDataPageHtml(String currency,String start,String end) {
    String generatedUrl = generateUrlForSpecificCurrency(currency,start,end)
    log.info("Sending GET request to ${generatedUrl}")
    HttpResponse response = getResponse(generatedUrl);
     EntityUtils.toString(response.getEntity());
  }

  private String generateUrlForSpecificCurrency(String currency, String start, String end) {
    if (currency.contains(' ')) {
      return indexPageUrl + 'currencies/' + currency.replace(' ', '-') + '/historical-data/?start=' + start.replace('-','') + '&end=' + end.replace('-','')
    }
     indexPageUrl + 'currencies/' + currency + '/historical-data/?start=' + start.replace('-', '') + '&end=' + end.replace('-','')
  }

  private HttpResponse getResponse(String url) {
    HttpGet request = new HttpGet(url);
     httpClient.execute(request)
  }

  private HistoricalData extractSingleDataForADay(Element row, String currency) {
    Elements cells = row.select('tr > td')

    if (!cells || cells.size() != cellsSize) {
      log.error("Unexpected structure found. Size of cells is not equal")
      return null
    }

    HistoricalData historicalData

    try {
      CryptocurrenciesEntity cryptocurrenciesEntity = cryptocurrenciesService.getEntityByName(currency)

      historicalData = new HistoricalData(
        localDate: parseDate(cells[0].text()),
        refID_Cryptocurrencies: cryptocurrenciesEntity.id,
        opened: parseFloat(cells[1].text()),
        high: parseFloat(cells[2].text()),
        low: parseFloat(cells[3].text()),
        closed: parseFloat(cells[4].text()),
        volume24h: parseFloat(cells[5].text()),
        marketCap: parseFloat(cells[6].text())
      )

    } catch (NumberFormatException e) {
      log.error("Failed to parse data - ${e.message}", e)
      historicalData = null
    } finally {
       historicalData
    }
  }

  private Float parseFloat(String text) {
     text
      .replace(',', '')
      .toFloat()
  }

  private LocalDate parseDate(String date) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern('MMM d yyyy')
     LocalDate.parse(
      date.replace(',', '').trim(), formatter)
  }
}
