package coinmarketcapbot.historicaldata

import coinmarketcapbot.cryptocurrencies.CryptocurrenciesEntity
import coinmarketcapbot.cryptocurrencies.CryptocurrenciesService
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j
import org.apache.groovy.json.internal.LazyMap
import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.util.EntityUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.apache.http.impl.client.HttpClientBuilder
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneOffset

@Service
@Slf4j
class HistoricalJsonParserService {

  private final String indexPageUrl = 'https://web-api.coinmarketcap.com/v1/cryptocurrency/ohlcv/historical?convert=USD&slug='

  private final HistoricalDataRepository historicalDataRepository

  private final CryptocurrenciesService cryptocurrenciesService

  private final JsonSlurper jsonSlurper = new JsonSlurper()

  private final HttpClient httpClient = HttpClientBuilder.create().build()

  @Autowired
  HistoricalJsonParserService(
    HistoricalDataRepository historicalDataRepository,
    CryptocurrenciesService cryptocurrenciesService
  ) {
    this.historicalDataRepository = historicalDataRepository
    this.cryptocurrenciesService = cryptocurrenciesService
  }

  void insert(String currency, String startDate, String endDate) {
    long startDateSeconds = parseDate(startDate)
    long endDateSeconds = parseDate(endDate)

    Map<String, LazyMap> map = getMapFromJson(currency, endDateSeconds, startDateSeconds)
    parseMapToHistoricalData(map, currency)
  }

  private HttpResponse getResponse(String url) {
    HttpGet getRequest = new HttpGet(url)
     httpClient.execute(getRequest)
  }

  private Map getMapFromJson(
    String currency,
    long endSeconds,
    long startSeconds
  ) {
    String generatedUrl = generateUrl(currency, endSeconds, startSeconds)
    HttpResponse response = getResponse(generatedUrl)
    String stringedResponse = EntityUtils.toString(response.getEntity())

    return jsonSlurper.parseText(stringedResponse)
  }

  private String generateUrl(
    String currency,
    long secondsEndDate,
    long secondsStartDate
  ) {
    if (currency.contains(' ')) {
      currency = currency.replace(' ', '-')
    }
     indexPageUrl + currency.toLowerCase() + '&time_end=' + secondsEndDate + '&time_start=' + secondsStartDate
  }

  private HistoricalData parseMapToHistoricalData(Map map, String currency) {

    ArrayList<LazyMap> listOfQuotes = map.get('data').get('quotes')

    for (LazyMap singleQuote : listOfQuotes) {
      LazyMap singleHistoricalDataJsonEntity = singleQuote.get('quote').get('USD')

      CryptocurrenciesEntity cryptocurrency = cryptocurrenciesService.getEntityByName(currency)

      if (!cryptocurrency) {
        log.info("No existing Cryptocurrency ${cryptocurrency.getName()}")
        return
      }

      HistoricalData historicalData = new HistoricalData(
        opened: singleHistoricalDataJsonEntity.get('open') as Float,
        high: singleHistoricalDataJsonEntity.get('high') as Float,
        localDate: filterTimestamp(singleHistoricalDataJsonEntity.get('timestamp')),
        low: singleHistoricalDataJsonEntity.get('low') as Float,
        closed: singleHistoricalDataJsonEntity.get('close') as Float,
        volume24h: singleHistoricalDataJsonEntity.get('volume') as Float,
        marketCap: singleHistoricalDataJsonEntity.get('market_cap') as Float,
        refID_Cryptocurrencies: cryptocurrency.id
      )

      HistoricalData data = this.historicalDataRepository.checkIfDataAlreadyExists(historicalData)
      if (data) {
        log.info("There is already information about the day ${data.getLocalDate()} Cryptorrency: ${data.refID_Cryptocurrencies}")
        continue
      }

      this.historicalDataRepository.createHistoricalDataForADay(historicalData)
      log.info("Persisted Information for day ${historicalData.localDate} Cryptocurrency ID: ${historicalData.refID_Cryptocurrencies}")
    }
  }

  private LocalDate filterTimestamp(String timestamp) {
    StringBuilder sb = new StringBuilder()
    while (true) {
      for (Character c : timestamp.toCharArray()) {
        if (Character.isLetter(c)) {
          return LocalDate.parse(sb.toString())
        }
        sb.append(c)
      }
    }
  }

  private long parseDate(String start) {
    LocalDateTime startDate = convertDate(start)
     startDate.toEpochSecond(ZoneOffset.UTC)
  }

  private LocalDateTime convertDate(String startDate) {
     LocalDate.parse(startDate).atStartOfDay()
  }
}
