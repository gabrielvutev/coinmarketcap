package coinmarketcapbot.historicaldata

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class HistoricalDataController {

  private final HistoricalDataParsingService historicalParsingService

  private final HistoricalJsonParserService jsonParserService

  @Autowired
  HistoricalDataController(
    HistoricalDataParsingService historicalParsingService,
    HistoricalJsonParserService jsonParserService
  ) {
    this.historicalParsingService = historicalParsingService
    this.jsonParserService = jsonParserService
  }

  @GetMapping('/historical-data/{currency}')
  List<HistoricalData> getHistoricalDataByStartEndDate(
    @PathVariable String currency,
    @RequestParam String startDate,
    @RequestParam String endDate
  ) {
    historicalParsingService.getHistoricalDataByStartEndDate(currency, startDate, endDate)
  }

  @GetMapping('historical-data-by-id/{id}')
  List<HistoricalData> getHistoricalDataById(
    @PathVariable Long id,
    @RequestParam String startDate,
    @RequestParam String endDate
  ) {
    historicalParsingService.getHistoricalDataById(id, startDate, endDate)
  }

  @GetMapping('historical-data-all-time/{currency}')
  List<HistoricalData> getHistoricalDataAllTime(
    @PathVariable String currency,
    @RequestParam String endDate
  ) {
    historicalParsingService.getHistoricalDataByCurrencyForAllTime(currency, endDate)
  }

  @PostMapping('/add-historical-data/{currency}')
  void insertHistoricalDataByName(
    @PathVariable String currency,
    @RequestParam String startDate,
    @RequestParam String endDate
  ) {
    historicalParsingService.parseHistoricalDataByName(currency, startDate, endDate)
  }

  @PostMapping('/add-historical-data-by-id/{id}')
  void insertHistoricalDataById(@PathVariable('id') Long currencyId) {
    historicalParsingService.parseHistoricalDataById(currencyId)
  }

  @GetMapping('/historical-data/all')
  List<HistoricalData> getFullHistoricalData() {
    historicalParsingService.getAllHistoricalData()
  }

  @GetMapping('historical-data-json/{currency}')
  void insertHistoricalDataJson(
    @PathVariable currency,
    @RequestParam startDate,
    @RequestParam endDate
  ) {
    this.jsonParserService.insert(currency, startDate, endDate)
  }
}
