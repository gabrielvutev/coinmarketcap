package coinmarketcapbot.historicaldata

import java.time.LocalDate

class HistoricalData {
  Long id
  Long refID_Cryptocurrencies
  LocalDate localDate
  Float opened
  Float high
  Float low
  Float closed
  Float volume24h
  Float marketCap
}
