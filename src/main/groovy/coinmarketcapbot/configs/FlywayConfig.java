package coinmarketcapbot.configs;

import org.flywaydb.core.Flyway;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

@Configuration
public class FlywayConfig {

  @Bean
  @Primary
  Flyway getFlyway(DataSource dataSource){
    Flyway flyway  = new Flyway();
    flyway.setDataSource(dataSource);
    return flyway;
  }

  @Bean
  @Primary
  FlywayMigrationInitializer getFlywayInitliazer(Flyway flyway){
    return new FlywayMigrationInitializer(flyway,null);
  }
}
