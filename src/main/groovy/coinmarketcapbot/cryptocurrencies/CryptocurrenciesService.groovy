package coinmarketcapbot.cryptocurrencies

import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
@Slf4j
class CryptocurrenciesService {

  private final CryptocurrenciesRepo repo

  @Autowired
  CryptocurrenciesService(CryptocurrenciesRepo cryptocurrenciesRepo) {
    this.repo = cryptocurrenciesRepo
  }

  List<CryptocurrenciesEntity> getAllCurrencies() {
    this.repo.getAllCurrencies()
  }

  CryptocurrenciesEntity getEntityByName(String name) {
     repo.getEntityIdByName(name)
  }

  void createNonExistingCryptocurrency(String name) {
    log.info("Persisting Crptocurrency with name ${name}")
    this.repo.insertNonExistingEntity(name)
  }

  CryptocurrenciesEntity getEntityById(Long id) {
    log.info("Trying to find cryptocurrency with id ${id}")
    CryptocurrenciesEntity cryptocurrenciesEntity = this.repo.getCryptocurrencyById(id)
    if (!cryptocurrenciesEntity) {
      log.info("Cryptocurrency with id ${id} does not exist in DB")
      return null
    }
     cryptocurrenciesEntity
  }
}
