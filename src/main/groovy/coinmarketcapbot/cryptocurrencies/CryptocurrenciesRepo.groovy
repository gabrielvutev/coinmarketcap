package coinmarketcapbot.cryptocurrencies

import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository

@Repository
@Slf4j
class CryptocurrenciesRepo {

  private final JdbcTemplate jdbcTemplate

  private final RowMapper<CryptocurrenciesEntity> rowMapper = new CryptocurrenciesEntityRowMapper()

  @Autowired
  CryptocurrenciesRepo(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate
  }

  List<CryptocurrenciesEntity> getAllCurrencies() {
    def sql =
      'EXECUTE getAllCryptocurrencies'

    jdbcTemplate.query(sql, rowMapper)
  }

  CryptocurrenciesEntity getEntityIdByName(String name) {
    def sql =
      'EXECUTE getIdCryptocurrencyByName @name=?'

    try {
      jdbcTemplate.queryForObject(sql, rowMapper, name)
    } catch (EmptyResultDataAccessException dataAccessException) {
      log.info("Entity with name ${name} has NOT been found " + dataAccessException)
      return null
    }
  }

  void insertNonExistingEntity(String name) {
    def sql =
      'EXECUTE insertNonExistingCryptocurrency @name=?'

    jdbcTemplate.update(sql, name)
  }

  CryptocurrenciesEntity getCryptocurrencyById(Long id) {
    def sql =
      'EXECUTE getCryptocurrencyById  @id=?'

    jdbcTemplate.queryForObject(sql, rowMapper, id)
  }
}
