package coinmarketcapbot.cryptocurrencies

import org.springframework.jdbc.core.RowMapper
import java.sql.ResultSet
import java.sql.SQLException

class CryptocurrenciesEntityRowMapper implements RowMapper<CryptocurrenciesEntity> {

  @Override
  CryptocurrenciesEntity mapRow(ResultSet resultSet, int i) throws SQLException {
    CryptocurrenciesEntity entity = new CryptocurrenciesEntity()

    entity.id = resultSet.getLong('id')
    entity.name = resultSet.getString('name')
    entity.ticker = resultSet.getString('ticker')
     entity
  }
}
