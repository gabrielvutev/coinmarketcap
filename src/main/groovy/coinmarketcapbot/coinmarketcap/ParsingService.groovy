package coinmarketcapbot.coinmarketcap

import coinmarketcapbot.cryptocurrencies.CryptocurrenciesEntity
import coinmarketcapbot.cryptocurrencies.CryptocurrenciesService
import groovy.util.logging.Slf4j
import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.util.EntityUtils
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
@Slf4j
class ParsingService {

  private final String indexPageUrl = 'https://coinmarketcap.com/'

  private final String row = 'cmc-table-row'

  private final String currencyNameHtmlElement = 'a[href])'

  private final int rowSize = 9

  private final CoinMarketCapService coinMarketCapService

  private final CryptocurrenciesService cryptocurrenciesService

  private final HttpClient httpClient = HttpClientBuilder.create().build()

  @Autowired
  ParsingService(
    CoinMarketCapService coinMarketCapService, CryptocurrenciesService cryptocurrenciesService
  ) {
    this.coinMarketCapService = coinMarketCapService
    this.cryptocurrenciesService = cryptocurrenciesService
  }

  void parseCoinMarketCapEntity(String name) {
    log.info("Starting for name ${name}")
    Elements rows = getAllIndexPageRows()
    if (!rows) return

    Element row = rows.find {
      el -> el.select(currencyNameHtmlElement).first()?.text()?.equalsIgnoreCase(name)
    }

    if (!row) {
      log.info("No such element with name ${name} exists \nPlease verify your input")
      return
    }

    CoinMarketCap entity = extractSingleElement(row)
    if (!row) return

    coinMarketCapService.createCoinMarketCap(entity)
  }

  void parseAllCoinMarketCaps() {
    log.info('Starting for all')
    Elements rows = getAllIndexPageRows()
    if (!rows) return

    rows.each { Element row ->
      CoinMarketCap entity = extractSingleElement(row)
      if (!entity) return
      coinMarketCapService.createCoinMarketCap(entity)
    }
    log.info('Finished for all')
  }

  private String getIndexPageHtml() {
    log.info("Sending GET request to ${indexPageUrl}")
    HttpResponse response = getResponse(indexPageUrl);
     EntityUtils.toString(response.getEntity());
  }

  private HttpResponse getResponse(String url) {
    HttpGet request = new HttpGet(url);
     httpClient.execute(request)
  }

  private Document getIndexPageDocument() {
    String htmlString = getIndexPageHtml()
     Jsoup.parse(htmlString)
  }

  private Elements getAllIndexPageRows() {
    Document document = getIndexPageDocument()
    Elements rows = document.getElementsByClass(row)
    if (!rows) {
      log.error("Failed to locate index page rows for url ${indexPageUrl}")
      return null
    }
     rows
  }

  private CoinMarketCap extractSingleElement(Element row) {
    Elements cells = row.select('tr > td')

    List<String> allValuesInOneEntityList = []

    if (!cells || cells.size() != rowSize) {
      log.error("Unexpected structure found. Size of cells ${cells.size()} is not equal row size ${rowSize}")
      return null
    }

    cells.each { cell ->
      String value = cell.text().replace('$', '').replace(',', '')
      allValuesInOneEntityList << value
    }

    CoinMarketCap entity

    try {
      String name = cells[1].text().trim()
      Float marketCap = parseFloat(cells[2].text())
      Float price = parseFloat(cells[3].text())
      Float volume24h = parseFloat(cells[4].text())
      Float change24h = parseFloat(cells[6].text())

      CryptocurrenciesEntity currency = getCryptocurrenciesEntityByName(name)
      entity = new CoinMarketCap(
        cryptocurrencyId: currency.id,
        marketCap: marketCap,
        price: price,
        volume24h: volume24h,
        change24h: change24h
      )
    } catch (NumberFormatException e) {
      log.error("Failed to parse data - ${e.message}", e)
      entity = null
    } finally {
       entity
    }

  }

  private CryptocurrenciesEntity getCryptocurrenciesEntityByName(String name) {
     cryptocurrenciesService.getEntityByName(name) ?: insertNotExistingCryptocurrenciesEntity(name)
  }

  private CryptocurrenciesEntity insertNotExistingCryptocurrenciesEntity(String name) {
    cryptocurrenciesService.createNonExistingCryptocurrency(name)
     cryptocurrenciesService.getEntityByName(name)
  }

  private static Float parseFloat(String text) {
     text
      .replace('$', '')
      .replace(',', '')
      .replace('%', '')
      .trim()
      .toFloat()
  }
}
