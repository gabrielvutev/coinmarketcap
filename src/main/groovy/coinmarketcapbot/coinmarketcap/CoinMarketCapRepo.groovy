package coinmarketcapbot.coinmarketcap

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository

@Repository
class CoinMarketCapRepo {

  private final JdbcTemplate jdbcTemplate

  private final RowMapper<CoinMarketCap> rowMapper = new CoinMarketCapEntityRowMapper();

  @Autowired
  CoinMarketCapRepo(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate
  }

  List<CoinMarketCap> retrieveWholeListOfCoins() {
    def sql =
      'EXECUTE getAllCoinMarketCapsEntities'

    jdbcTemplate.query(sql, rowMapper);
  }

  void createCoinMarketCap(CoinMarketCap entity) {
    def sql = '''
      EXECUTE createCoinMarketCap
       @refID_Cryptocurrencies=?,
       @marketCap=?,
       @price=?,
       @volume24H=?,
       @change24H=?'''

    jdbcTemplate.update(
      sql,
      entity.cryptocurrencyId,
      entity.marketCap,
      entity.price,
      entity.volume24h,
      entity.change24h
    )
  }

}
