package coinmarketcapbot.coinmarketcap

import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@Slf4j
class CoinMarketCapController {

  private final CoinMarketCapService coinMarketCapService

  private final ParsingService parsingService

  @Autowired
  CoinMarketCapController(CoinMarketCapService service, ParsingService parsingService
  ) {
    this.coinMarketCapService = service;
    this.parsingService = parsingService
  }

  @GetMapping('/coinmarketcaps')
  List<CoinMarketCap> getCoinMarketCaps() {
     coinMarketCapService.getEntities();
  }

  @PostMapping('/add')
  void insertAnEntity(@RequestParam(value = 'name', required = false) String nameOfEntity) {
    parsingService.parseCoinMarketCapEntity(nameOfEntity)
  }
}
