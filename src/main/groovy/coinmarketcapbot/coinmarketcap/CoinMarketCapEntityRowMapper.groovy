package coinmarketcapbot.coinmarketcap

import org.springframework.jdbc.core.RowMapper
import java.sql.ResultSet
import java.sql.SQLException

class CoinMarketCapEntityRowMapper implements RowMapper<CoinMarketCap> {

  @Override
  CoinMarketCap mapRow(ResultSet resultSet, int i) throws SQLException {
    CoinMarketCap entity = new CoinMarketCap();

    entity.id = resultSet.getLong('id')
    entity.cryptocurrencyId = resultSet.getLong('refID_Cryptocurrencies')
    entity.marketCap = resultSet.getFloat('marketCap')
    entity.price = resultSet.getFloat('price')
    entity.volume24h = resultSet.getFloat("volume24h")
    entity.change24h = resultSet.getFloat("change24h")
    entity.creationDate = resultSet.getDate("creationDate")

     entity
  }

}
