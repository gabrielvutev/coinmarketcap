package coinmarketcapbot.coinmarketcap

import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Profile
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
@Slf4j
@Profile("secrets")
class ScheduledTasks {

  private final ParsingService parsingService;

  @Value('${scheduledTasks.coinMarketCap.realtime.active}')
  private final boolean isActive

  @Autowired
  ScheduledTasks(ParsingService parsingService) {
    this.parsingService = parsingService
  }

  @Scheduled(cron = '${scheduledTasks.coinMarketCap.realtime.cron}')
  void insertPerTime() {
    if (!isActive) {
      return
    }
    log.info('Scheduled: Starting realtime scan')
    parsingService.parseAllCoinMarketCaps()
    log.info('Scheduled: Finished realtime scan')
  }

}
