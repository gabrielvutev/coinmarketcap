package coinmarketcapbot.coinmarketcap

import java.sql.Date

class CoinMarketCap {
  Long id
  Long cryptocurrencyId
  Float marketCap
  Float price
  Float volume24h
  Float change24h
  Date creationDate
}
