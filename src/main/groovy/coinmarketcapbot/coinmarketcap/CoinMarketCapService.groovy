package coinmarketcapbot.coinmarketcap

import coinmarketcapbot.cryptocurrencies.CryptocurrenciesRepo
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
@Slf4j
class CoinMarketCapService {

  private final CoinMarketCapRepo coinMarketCapRepo

  private final CryptocurrenciesRepo cryptocurrenciesRepo

  @Autowired
  CoinMarketCapService(CoinMarketCapRepo repo, CryptocurrenciesRepo cryptocurrenciesRepo
  ) {
    this.coinMarketCapRepo = repo
    this.cryptocurrenciesRepo = cryptocurrenciesRepo
  }

  List<CoinMarketCap> getEntities() {
    this.coinMarketCapRepo.retrieveWholeListOfCoins();
  }

  void createCoinMarketCap(CoinMarketCap coinMarketCap) {
    if (!coinMarketCap) {
      return
    }
    log.info("Persisting market cap data for Cryptocurency ID: ${coinMarketCap.cryptocurrencyId}")
    this.coinMarketCapRepo.createCoinMarketCap(coinMarketCap)
  }

}
