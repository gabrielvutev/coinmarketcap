IF EXISTS( SELECT 1 FROM CryptoDB.sys.objects WHERE name = 'getIdCryptocurrencyByName')
BEGIN
DROP PROCEDURE[getIdCryptocurrencyByName]
END
GO
CREATE PROCEDURE[getIdCryptocurrencyByName]
@name varchar(100)

  AS
    BEGIN
       SELECT
       id,
       name,
       ticker,
       marketCap
    FROM dbo.[Cryptocurrencies]
    WHERE name = @name
END
