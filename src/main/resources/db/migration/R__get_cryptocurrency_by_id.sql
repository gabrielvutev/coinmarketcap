IF EXISTS( SELECT 1 FROM CryptoDB.sys.objects WHERE name = 'getCryptocurrencyById')
BEGIN
DROP PROCEDURE[getCryptocurrencyById]
END
GO
CREATE PROCEDURE[getCryptocurrencyById]
@id INT
  AS
  BEGIN
    SELECT
      id,
      name,
      ticker,
      marketCap
    FROM dbo.Cryptocurrencies
    WHERE id = @id

 END
