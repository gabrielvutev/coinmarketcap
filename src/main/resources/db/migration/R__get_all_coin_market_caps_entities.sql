IF EXISTS( SELECT 1 FROM CryptoDB.sys.objects WHERE name = 'getAllCoinMarketCapsEntities')
BEGIN
DROP PROCEDURE[getAllCoinMarketCapsEntities]
END
GO
CREATE PROCEDURE[getAllCoinMarketCapsEntities]
  AS
    BEGIN
      SELECT
        id,
        refID_Cryptocurrencies,
        marketCap,
        price,
        volume24h,
        change24h,
        creationDate
       FROM CryptoCurrencyCap
END
