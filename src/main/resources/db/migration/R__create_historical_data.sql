IF EXISTS( SELECT 1 FROM CryptoDB.sys.objects WHERE name = 'createHistoricalData')
BEGIN
DROP PROCEDURE[createHistoricalData]
END
GO
CREATE PROCEDURE[createHistoricalData]
  @refID_Cryptocurrencies INT,
  @datetime DATE,
  @opened FLOAT,
  @closed FLOAT,
  @high FLOAT,
  @low FLOAT,
  @volume24h FLOAT,
  @marketCap FLOAT

AS
BEGIN
    INSERT INTO dbo.HistoricalData(
    refID_Cryptocurrencies,
    datetime,
    opened,
    closed,
    high,
    low,
    volume24h,
    marketCap
    )
    SELECT @refID_Cryptocurrencies, @datetime, @opened, @closed, @high, @low, @volume24h, @marketCap

END


