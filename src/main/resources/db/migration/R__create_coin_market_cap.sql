IF EXISTS( SELECT 1 FROM CryptoDB.sys.objects WHERE name = 'createCoinMarketCap')
BEGIN
DROP PROCEDURE[createCoinMarketCap]
END
GO
CREATE PROCEDURE[createCoinMarketCap]
@refID_Cryptocurrencies INT,
@marketCap FLOAT,
@price FLOAT,
@volume24H FLOAT,
@change24H FLOAT

  AS
    BEGIN
      INSERT INTO [dbo].[CryptoCurrencyCap](
          refID_Cryptocurrencies,
          marketCap,
          price,
          volume24H,
          change24H
        )
        VALUES (
       @refID_Cryptocurrencies,
        @marketCap,
        @price,
        @volume24H,
        @change24H
        )
END

