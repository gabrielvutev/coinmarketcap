IF EXISTS( SELECT 1 FROM CryptoDB.sys.objects WHERE name = 'insertNonExistingCryptocurrency')
BEGIN
DROP PROCEDURE[insertNonExistingCryptocurrency]
END
GO
CREATE PROCEDURE[insertNonExistingCryptocurrency]
@name varchar(100)

AS
BEGIN
    INSERT INTO [dbo].[Cryptocurrencies](
      name
    ) VALUES (@name)

END
