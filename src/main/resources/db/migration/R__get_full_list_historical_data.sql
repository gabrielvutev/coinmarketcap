IF EXISTS( SELECT 1 FROM CryptoDB.sys.objects WHERE name = 'getFullListHistoricalData')
BEGIN
DROP PROCEDURE[getFullListHistoricalData]
END
GO
CREATE PROCEDURE[getFullListHistoricalData]
AS
  BEGIN
  SELECT
    id,
    refID_Cryptocurrencies,
    datetime,
    opened,
    high,
    low,
    closed,
    volume24h,
    marketCap

    FROM HistoricalData
END
