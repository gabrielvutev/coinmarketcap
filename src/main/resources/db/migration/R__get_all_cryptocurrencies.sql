IF EXISTS( SELECT 1 FROM CryptoDB.sys.objects WHERE name = 'getAllCryptocurrencies')
BEGIN
DROP PROCEDURE[getAllCryptocurrencies]
END
GO
CREATE PROCEDURE[getAllCryptocurrencies]

AS
  BEGIN
    SELECT
     id,
     name,
     ticker
   FROM Cryptocurrencies
  END
