IF EXISTS( SELECT 1 FROM CryptoDB.sys.objects WHERE name = 'checkForExistingHistData')
BEGIN
DROP PROCEDURE[checkForExistingHistData]
END
GO
CREATE PROCEDURE[checkForExistingHistData]
@datetime DATE,
@refID_Cryptocurrencies INT

AS
BEGIN
  SELECT
      id,
      refID_Cryptocurrencies,
      datetime,
      opened,
      high,
      low,
      closed,
      volume24h,
      marketCap

  FROM dbo.HistoricalData
  WHERE datetime = @datetime
  AND refID_Cryptocurrencies = @refID_Cryptocurrencies

END
