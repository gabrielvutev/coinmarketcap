
CREATE TABLE Cryptocurrencies(
    	id INT Primary key Identity(1,1),
    	name varchar(100),
    	ticker varchar(100),
		marketCap float
)



CREATE TABLE CryptoCurrencyCap (
   	id INT  Identity(1,1),
   	refID_Cryptocurrencies INT FOREIGN KEY REFERENCES Cryptocurrencies(id),
   	marketCap float,
   	price float,
   	volume24h float,
   	change24h float,
   	creationDate DATETIME DEFAULT GETDATE()
   )



     CREATE TABLE HistoricalData(
  id INT  Identity(1,1),
  refID_Cryptocurrencies INT,
  datetime DATE,
  opened Float,
  high Float,
  low Float,
  closed Float,
   volume24h Float,
   marketCap Float
  )
