IF EXISTS( SELECT 1 FROM CryptoDB.sys.objects WHERE name = 'getHistoricalDataByDate')
BEGIN
DROP PROCEDURE[getHistoricalDataByDate]
END
GO
CREATE PROCEDURE[getHistoricalDataByDate]
@refID_Cryptocurrencies INT,
@startDate DATE,
@endDate DATE

AS
  BEGIN
    SELECT
    id,
    refID_Cryptocurrencies,
    datetime,
    opened,
    high,
    low,
    closed,
    volume24h,
    marketCap
    FROM dbo.HistoricalData
    WHERE refID_Cryptocurrencies = @refID_Cryptocurrencies
    AND datetime BETWEEN @startDate AND @endDate

END
